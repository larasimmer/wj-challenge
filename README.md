# WEBJUMP FRONTEND CHALLENGE
O desafio consiste em tornar o layout proposto em uma página funcional.

## Tecnologias adotadas

- jQuery: Framework que me permitia manipular mais facilmente os elementos, e é o que eu tenho mais domínio no momento.

- AJAX: biblioteca para fazer requisições de API.

- Scss: escolhi utilizar este pré-processador para manter a manipulação do CSS mais organizada e dinâmica.

## Passo a passo para executar o projeto
1. Clonar o repósitório
2. Acessar o localhost:8888 no navegador

## Requisitos
- Design responsivo nos breakpoints 320px, 768px, 1024px e 1440px: OK
- Semântica: OK
- JavaScript para consultar a lista de categorias a serem exibidas no menu e lista de produtos das categorias: OK
- Fonte padrão: "Open Sans" e "Open Sans - Extrabold": OK
- Suporte para IE, Chrome, Safari, Firefox: Suporte apenas para Chrome e Firefox

## Diferenciais
- Uso de pré-processadores CSS (Sass, Less): OK
- Fazer os filtros da sidebar funcionarem através de Javascript: OK
