$(document).ready(async function() {

//VARIAVEIS/////////////////////////////////////////////////////

    let categoriaAtual;
    let categorias = [];
    let produtos = [];
    let cores = [];
    let coresUnicas = [];
    let generos = [];
    let generosUnicos = [];


//CHAMADAS DAS FUNCOES/////////////////////////////////////////////////////

    await lerCategorias();
    escreverCategorias();
    await lerProdutos();
    escreverTitulo();
    escreverBreadcrumbs();
    escreverProdutos(produtos);
    definirFiltros();
    lerCores();
    removerCoresDuplicadas();
    escreverCores();
    lerGeneros();
    removerGenerosDuplicados();
    escreverGeneros();
    clicarCategoriaDoFiltro();
    clicarCorDoFiltro();
    clicarGeneroDoFiltro();
 
//DECLARAR FUNCOES/////////////////////////////////////////////////////

    async function lerCategorias() {
        let categoriaId = localStorage.getItem("categoriaId");
        await $.ajax({
            type:"GET",
            url:"http://localhost:8888/api/V1/categories/list",
            success:function(response) {
                categorias = [];
                response.items.forEach(categoria => {
                    categorias.push(categoria);
                    if (categoriaId == categoria.id) {
                        categoriaAtual = categoria;
                    }
                });
            },
            error:function() {
                alert("Ocorreu um erro");
            }
        });
    }

    function escreverCategorias() {
        categorias.forEach(categoria => {
            $(".filtros__lista").append(`<li data-categoria='${categoria.id}' class='filtros__item'>${categoria.name}</li>`);
        });
    }

    async function lerProdutos() {
        await $.ajax({
            type:"GET",
            url:`http://localhost:8888/api/V1/categories/${categoriaAtual.id}`,
            success:function(response) {
                produtos = [];
                response.items.forEach(produto => {
                    produtos.push(produto);
                });
            },
            error:function() {
                alert("Ocorreu um erro");
            }
        });
    }

    function escreverTitulo() {
        $(".catalogo__titulo").text(`${categoriaAtual.name}`);
    }

    function escreverBreadcrumbs() {
        $(".main__breadcrumbs").append(`<li class='breadcrumbs__item' data-categoria='${categoriaAtual.id}' style='color: #de0020'>${categoriaAtual.name}</li>`);
    }

    function escreverProdutos(arrayDeProdutos) {
        $(".catalogo__lista").html("");
        let divProduto;
        arrayDeProdutos.forEach(produto => {
            let precoProduto = produto.price.toLocaleString("pt-BR", {style:"currency", currency:"BLR"}).replace("BLR", "R$");
            
            if (typeof produto.specialPrice !== "undefined") {
                let precoEspecial = produto.specialPrice.toLocaleString("pt-BR", {style:"currency", currency:"BLR"}).replace("BLR", "R$");
                divProduto =`<div class='catalogo__item item'>
                                            <div class="item__container-imagem">
                                                <img class="item__imagem" src="http://localhost:8888/${produto.image}"/>
                                            </div>
                                            <div class="item__informacoes">
                                                <div class="item__texto">
                                                    <h4 class="item__nome">${produto.name}</h4>
                                                </div>
                                                <div class="item__precos">
                                                    <h5 class="item__preco">${precoProduto}</h5>
                                                    <h5 class="item__preco-especial">${precoEspecial}</h5>
                                                    
                                                </div>
                                                
                                                <button class="item__button">Comprar</button>
                                            </div>
                                        </div>`;
                         } else {
                                divProduto =`<div class='catalogo__item item'>
                                                <div class="item__container-imagem">
                                                    <img class="item__imagem" src="http://localhost:8888/${produto.image}"/>
                                                </div>
                                                <div class="item__informacoes">
                                                    <div class="item__texto">
                                                        <h4 class="item__nome">${produto.name}</h4>
                                                    </div>
                                                    <div class="item__precos">
                                                        <h5 class="item__preco-especial">${precoProduto}</h5>
                                                    </div>
                                                    
                                                    <button class="item__button">Comprar</button>
                                                 </div>
                                            </div>`;
                                }
            $(".catalogo__lista").append(divProduto);
        });
    }

    function definirFiltros() {
        let filtros;
        if (categoriaAtual.name == "Camisetas" || categoriaAtual.name == "Calçados") {
            filtros = `<h3 class="filtros__subtitulo">Cores</h3>
                        <ul class="filtros__cores"></ul>`
        } else if (categoriaAtual.name == "Calças") {
            filtros = `<h3 class="filtros__subtitulo">Gêneros</h3>
                        <ul class="filtros__generos"></ul>`
        } 

        $(".main__filtros").append(filtros);
    }

    function lerCores() {
        cores = [];
        produtos.forEach(produto => {
            if (produto.filter.length !== 0) {
                produto.filter.forEach(item => {
                    if (typeof item.color !== "undefined") {
                        cores.push(item.color);
                    }
                });  
            } 
        });
    }

    function removerCoresDuplicadas() {
        cores.forEach(cor => {
            if (!coresUnicas.includes(cor)) {
                coresUnicas.push(cor);
            }
        });
        return coresUnicas;
    }

    function escreverCores() {
        $(".filtros__cores").html("");
        let corTraduzida = "";
        coresUnicas.forEach(cor => {
            if (cor == "Preta" || cor == "Preto" || cor == "preta" || cor == "preto") {
                corTraduzida = "black";
            } else if (cor == "Rosa" || cor == "rosa") {
                corTraduzida = "#F22973";
            } else if (cor == "Laranja" || cor == "laranja") {
                corTraduzida = "#E65100";
            } else if (cor == "Amarelo" || cor == "Amarela" || cor == "amarelo" || cor == "amarela") {
                corTraduzida = "#F2CE16";
            } else if (cor == "Bege" || cor == "bege") {
                corTraduzida = "#DAB67A";
            } else if (cor == "Cinza" || cor == "cinza") {
                corTraduzida = "gray";
            } else if (cor == "Azul" || cor == "azul") {
                corTraduzida = "#4E7AC7";
            } 
            $(".filtros__cores").append(`<li class='filtros__cor' data-cor='${cor}'><button class='filtros__button' style="background-color:${corTraduzida};color:${corTraduzida}">${cor}</button></li>`);
        });
    }

    function lerGeneros() {
        generos = [];
        produtos.forEach(produto => {
            if (produto.filter.length !== 0) {
                produto.filter.forEach(item => {
                    if (typeof item.gender !== "undefined") {
                        generos.push(item.gender);
                    }
                });  
            } 
        });
    }

    function removerGenerosDuplicados() {
        generos.forEach(genero => {
            if (!generosUnicos.includes(genero)) {
                generosUnicos.push(genero);
            }
        });
        return generosUnicos;
    }
    
    function escreverGeneros() {
        $(".filtros__generos").html("");
        generosUnicos.forEach(genero => {
            $(".filtros__generos").append(`<li class='filtros__item' data-genero='${genero}'>${genero}</li>`);
        });
    }

    function clicarCategoriaDoFiltro() {
        $(".filtros__item").on("click", function() {
            let categoriaId = $(this).data("categoria");
            if (typeof categoriaId !== "undefined") {
                localStorage.setItem("categoriaId", categoriaId);
                setTimeout(function() {
                    window.location = "catalogo.html";
                },100);
            }
        });
    }

    function clicarCorDoFiltro() {
        $(".filtros__cor").on("click", function() {
            let cor = $(this).data("cor");
            let novoArrayProdutos = [];
            produtos.forEach(produto => {
               produto.filter.forEach(filter => {
                   if (typeof filter.color !== "undefined") {
                       if (filter.color == cor) {
                           novoArrayProdutos.push(produto);
                       }
                   }
               });
            });
            setTimeout(function() {
                escreverProdutos(novoArrayProdutos);
            },100);
        });
           
    }

    function clicarGeneroDoFiltro() {
        $(".filtros__item").on("click", function() {
            let genero = $(this).data("genero");
            let novoArrayProdutos = [];
            produtos.forEach(produto => {
               produto.filter.forEach(filter => {
                   if (typeof filter.gender !== "undefined") {
                       if (filter.gender == genero) {
                           novoArrayProdutos.push(produto);
                       }
                   }
               });
            });
            setTimeout(function() {
                escreverProdutos(novoArrayProdutos);
            },100);
        });
           
    }
});
