$(document).ready(async function() {

//VARIAVEIS/////////////////////////////////////////////////////
    let categorias = [];


//CHAMADAS DAS FUNCOES/////////////////////////////////////////////////////
    itemPaginaInicial();
    await lerCategorias();
    escreverCategorias();
    itemContato();
    clicarCategoria();

    
//DECLARAR FUNCOES/////////////////////////////////////////////////////
    async function lerCategorias() {
        await $.ajax({
            type:"GET",
            url:"http://localhost:8888/api/V1/categories/list",
            success:function(response) {
                categorias = [];
                response.items.forEach(categoria => {
                    categorias.push(categoria);
                });
            },
            error:function() {
             alert("Ocorreu um erro");
            }
        });
    }

    function escreverCategorias() {
        categorias.forEach(categoria => {
            $(".navbar__lista").append(`<li data-categoria='${categoria.id}' class='navbar__item item'>${categoria.name}</li>`);
        });
    }

    function itemPaginaInicial() {
        $(".navbar__lista").append("<li class='navbar__item item'><a href='index.html' class='item__a'>Página inicial</a></li>");
    }

    function itemContato() {
        $(".navbar__lista").append("<li class='navbar__item item'>Contato</li>");
    }

    function clicarCategoria() {
        $(".navbar__item").on("click", function() {
            let categoriaId = $(this).data("categoria");
            if (typeof categoriaId !== "undefined") {
                localStorage.setItem("categoriaId", categoriaId);
                setTimeout(function() {
                    window.location = "catalogo.html";
                },100);
            }
        });
    }
});